# Presentation et Travaux Pratiques GUDHI à la [Journée Jeunes Chercheurs en Géométrie](https://jcgeo24.sciencesconf.org/)

## Introduction

L'analyse topologique des données (TDA) est une branche relativement récente de l'analyse de données qui découvre et exploite des informations de nature géométrique et topologique dans les données.

Ces mêmes informations fournissent une approche puissante pour déduire des informations qualitatives et parfois quantitatives et robustes sur la structure des données. Ces méthodes permettent de déduire, analyser et exploiter des données complexes (nuages ​​de points, graphiques, images, formes 3D, séries temporelles...).

[GUDHI](https://gudhi.inria.fr/) est une librairie open source efficace et facile à utiliser qui fournit les structures de données et les algorithmes dédiés à la TDA.

Après une courte présentation, nous vous proposons une série de travaux pratiques pour mettre le pied à l'étrier de la TDA et écrire
vos premières pipelines de TDA avec [GUDHI](https://gudhi.inria.fr/).

* **Pré-requis:** avoir des notions de Python
* **Installation:** `pip install -r requirements.txt`
(le mieux étant de travailler dans un [environnement conda](https://docs.anaconda.com/free/miniconda/miniconda-install/) dédié, par exemple, avec un python récent > 3.8)

## Ressources

* [Le projet GUDHI](https://gudhi.inria.fr/)
* [La documentation de l'interface Python pour GUDHI](https://gudhi.inria.fr/python/latest/)
* [Le projet sur GitHub](https://github.com/GUDHI/gudhi-devel) - N'hésitez pas à [ouvrir des issues](https://github.com/GUDHI/gudhi-devel/issues/new) si vous trouvez des points d'améliorations (documentation, bugs, ...)
* [Une mailing list](https://sympa.inria.fr/sympa/arc/gudhi-users) - N'hésitez pas à consulter les archives et à vous inscrire

## Présentation

https://gudhi.gitlabpages.inria.fr/Presentation_gustave_eiffel/

## Travaux Pratiques

```bash
git clone https://gitlab.inria.fr/GUDHI/Presentation_gustave_eiffel.git
cd Presentation_gustave_eiffel
pip install -r requirements.txt
cd practical
jupyter lab
```

A vous de jouer !