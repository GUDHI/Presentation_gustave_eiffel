import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import gudhi as gd
from gudhi.datasets.generators import points
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--no-show', action='store_true', default=False)
parser.add_argument('--no-img', action='store_true', default=False)
args = parser.parse_args()

N=20
X = points.sphere(n_samples = N, ambient_dim = 2)
fig, axes = plt.subplots(ncols=2, figsize=(4, 2))

scat = axes[0].scatter(X[:,0],X[:,1], s=1, color='green')
axes[0].set_xlim(-2., 2.)
axes[0].set_ylim(-2., 2.)

stree = gd.AlphaComplex(points=X).create_simplex_tree()
dgm = stree.persistence()

gd.plot_persistence_barcode(dgm, legend=True, axes=axes[1])
pgrs_line, = axes[1].plot([0., 0.], [24., 0.], color='green', linestyle='dotted')

def updata(frame_number):
    scat.set_sizes(frame_number * frame_number * np.ones(N))
    pgrs_line_val = math.pow(1.1 * frame_number, 2.8) / 4000000.
    pgrs_line.set_data([pgrs_line_val, pgrs_line_val], [24., 0.])
    return pgrs_line,


anim = animation.FuncAnimation(fig,updata,frames=250)
fig.set_size_inches(19, 10)

if args.no_img == False:
    # fps = 300 gives a proper 'gif' speed
    f = r"./img/pointcloud_sublevelsets.gif"
    writergif = animation.PillowWriter(fps=300)
    anim.save(f, writer=writergif)
    
    plt.savefig('./img/pointcloud_sublevelsets.png')

if args.no_show == False:
    plt.show()
