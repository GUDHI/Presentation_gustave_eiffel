import matplotlib.pyplot as plt
import numpy as np
from gudhi import plot_persistence_diagram
from gudhi.representations import Landscape, Silhouette, PersistenceImage
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--no-show', action='store_true', default=False)
parser.add_argument('--no-img', action='store_true', default=False)
args = parser.parse_args()

D1 = np.array([[0., 0.5], [0.125, 0.25], [0.375, 0.9], [0.75, 0.9]])

fig, axes = plt.subplots(nrows=2, ncols=3)

axes[0][0].axis('off')
axes[0][2].axis('off')

plot_persistence_diagram(D1, axes=axes[0][1])
axes[0][1].set_aspect('equal')

LS = Landscape(resolution=1000)
L = LS(D1)
axes[1][0].plot(L[:1000])
axes[1][0].plot(L[1000:2000])
axes[1][0].plot(L[2000:3000])
axes[1][0].set_title("Landscape")
axes[1][0].set_aspect(1000/0.4)
axes[1][0].yaxis.set_visible(False)

def pow(n):
  return lambda x: np.power(x[1]-x[0],n)

SH = Silhouette(resolution=1000, weight=pow(2))
axes[1][1].plot(SH(D1))
axes[1][1].set_title("Silhouette")
axes[1][1].set_aspect(1000/0.18)
axes[1][1].yaxis.set_visible(False)

PI = PersistenceImage(bandwidth=.1, weight=lambda x: x[1], im_range=[0,1,0,1], resolution=[100,100])
axes[1][2].imshow(np.flip(np.reshape(PI(D1), [100,100]), 0))
axes[1][2].set_title("Persistence Image")
axes[1][2].set_aspect('equal')
axes[1][2].yaxis.set_visible(False)

if args.no_img == False:
    fig.set_size_inches(19, 10)
    plt.savefig('./img/representations.png', dpi=100)

if args.no_show == False:
    plt.show()