import matplotlib.pyplot as plt
import numpy as np
import gudhi as gd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--no-show', action='store_true', default=False)
parser.add_argument('--no-img', action='store_true', default=False)
args = parser.parse_args()

fig, (axl, axm) = plt.subplots(
    ncols=2,
    sharey=True,
    figsize=(7, 1),
    gridspec_kw=dict(width_ratios=[4, 3], wspace=0),
)
axm.yaxis.set_visible(False)
axm.xaxis.set_visible(False)

end_x = 1.3

t = np.arange(-0.1, end_x, 0.01)
s = np.sin(2 * np.pi * t) - t * np.cos(6 * np.pi * t) + 2

axl.plot(t, s, "k", lw=0.5)
axl.set_ylim(0, 4.5)
axl.set_yticks([0, 1, 2, 3, 4])

noisy = s + (np.random.random(140) - 0.5) * 0.3
axl.scatter(t, noisy, c='red', marker='o', alpha=0.6)

cplx = gd.CubicalComplex(top_dimensional_cells=s)
diag = cplx.persistence()
# Change "dimension" for display reasons
diag = [(1, val[1]) for val in diag]

cplx_noisy = gd.CubicalComplex(top_dimensional_cells=noisy)
diag_noisy = cplx_noisy.persistence()

gd.plot_persistence_diagram(diag + diag_noisy, legend=True, axes=axm)
axm.set_title('')
legend = axm.get_legend()
legend.get_texts()[0].set_text('signal + noise')
legend.get_texts()[1].set_text('signal')

if args.no_img == False:
    fig.set_size_inches(19, 10)
    plt.savefig('./img/stability.png', dpi=100)

if args.no_show == False:
    plt.show()