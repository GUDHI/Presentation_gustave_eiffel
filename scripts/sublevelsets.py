import matplotlib.pyplot as plt
import numpy as np
import gudhi as gd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--no-show', action='store_true', default=False)
parser.add_argument('--no-img', action='store_true', default=False)
args = parser.parse_args()

fig, (axl, axm) = plt.subplots(
    ncols=2,
    sharey=True,
    figsize=(6, 1),
    gridspec_kw=dict(width_ratios=[4, 2], wspace=0),
)
axm.yaxis.set_visible(False)
axm.xaxis.set_visible(False)

end_x = 1.3
# draw with initial point in left Axes
t = np.arange(-0.1, end_x, 0.01)
s = np.sin(2 * np.pi * t) - t * np.cos(6 * np.pi * t) + 2

axl.plot(t, s, 'k', lw=0.5)
plt.ylim(0,4.4)

# (0.43653, inf)
# (2.08551, 4.04775)
# (1.38131, 3.11285)
# (0.96348, 1.99938)
# (2.44232, 2.58421)

# Inf is 4.4 here
bar_max = [4.4,    4.04775, 3.11285, 1.99938, 2.58421]
bar_min = [0.4365, 2.08551, 1.38131, 0.96348, 2.44232]

barcode = axm.bar(range(1, 6), np.subtract(bar_max, bar_min), bottom = bar_min)

if args.no_img == False:
    fig.set_size_inches(19, 10)
    plt.savefig('./img/sublevelsets.png', dpi=100)

if args.no_show == False:
    plt.show()
