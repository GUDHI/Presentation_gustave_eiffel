import matplotlib.pyplot as plt
import numpy as np

import matplotlib.animation as animation
from matplotlib.patches import ConnectionPatch
from matplotlib.lines import Line2D
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--no-show', action='store_true', default=False)
parser.add_argument('--no-img', action='store_true', default=False)
args = parser.parse_args()

fig, (axl, axr) = plt.subplots(
    ncols=2,
    sharey=True,
    figsize=(6, 2),
    gridspec_kw=dict(width_ratios=[3, 1], wspace=0),
)
#axl.xaxis.set_visible(False)
axr.yaxis.set_ticks_position('right')
axr.xaxis.set_visible(False)

end_x = 1.3
# draw with initial point in left Axes
t = np.arange(0.0, end_x, 0.01)
s = np.sin(2 * np.pi * t) - t * np.cos(6 * np.pi * t) + 2

start_y = 4.5
axl.plot(t, s, "k", lw=0.5)

pointl, = axl.plot(0, start_y, "o")

# draw full curve to set view limits in right Axes
bar_max = [4.04, 3.11, 2.58, 2]
bar_min = [0,    0.43, 2.44, 0.96]
barcode = axr.bar(range(1, 5), [0, 0, 0, 0], bottom = bar_min)
pointr, = axr.plot(5, start_y, "o")

con = ConnectionPatch(
    (0, start_y),
    (5, start_y),
    "data",
    "data",
    axesA=axl,
    axesB=axr,
    color="C0",
    ls="dotted",
)
fig.add_artist(con)

pause = False

def animate(i):
    global pause
    if pause == False:
        # new_yval is a numpy array single value
        new_yval = pointl.get_ydata() - 0.01
        pointl.set_ydata(new_yval)
        pointr.set_ydata(new_yval)
        # From here new_yval is just a single value
        if isinstance(new_yval, np.ndarray):
            new_yval = new_yval[0]
        if new_yval <= 0.:
            pause = True
        idx = 0
        for b in barcode:
            idx += 1
            if new_yval < bar_max[idx-1] and new_yval > bar_min[idx-1]:
                b.set_y(new_yval)
                b.set_height(bar_max[idx-1] - new_yval)
        con.xy1 = (0, new_yval)
        con.xy2 = (5, new_yval)
    return pointl, pointr, con

# (number of) frames = 4.5 / 0.01
# interval = 50 gives a proper 'matplotlib' speed
anim = animation.FuncAnimation(fig, animate, frames=450, interval=50, blit=False, repeat=False)

if args.no_img == False:
    # fps = 300 gives a proper 'gif' speed
    f = r"./img/superlevelsets.gif"
    writergif = animation.PillowWriter(fps=300)
    anim.save(f, writer=writergif)
    
    plt.savefig('./img/superlevelsets.png')

if args.no_show == False:
    plt.show()
